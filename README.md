Ear Wax Clear is a leading ear and hearing clinic in Sheffield. Our highly respected audiologist and hearing aid dispenser Peter Byrom has successfully and safely cleared the ears of over 1,000 clients and has many years of experience.

Website: https://ear-wax-clear.co.uk
